using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;


[System.Serializable]
public class DefaultRandomPasswordGenerator : RandomPasswordGenerator
{
    public uint m_minChar = 12, m_maxChar = 256;
    public string m_charAllowsDefault = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!?";
    public string m_charAllowsSpecial = "aqwzsxedcrfvtgbyhnuj,ik;ol:pm=^�$�1023456789AQWZSXEDCRFVTGBYHNUJKIOLMP�%�*+/.?";
    public string GetRandomPassword()
    {
        return GetRandomPasswordFrom(m_charAllowsDefault);
    }
    public string GetRandomPasswordSpecial()
    {
        return GetRandomPasswordFrom(m_charAllowsSpecial);
    }

    private char GetRandomChar(string characters)
    {
        int i = UnityEngine.Random.Range(0, characters.Length);
        return characters[i];
    }

    public string GetRandomPasswordFrom(string allowChars)
    {
        StringBuilder sb = new StringBuilder();
        int ccount = (int)UnityEngine.Random.Range(m_minChar, m_maxChar);
        for (int i = 0; i < ccount; i++)
        {
            sb.Append(GetRandomChar(allowChars));
        }
        return sb.ToString();
    }
}