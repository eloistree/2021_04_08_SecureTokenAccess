using EncryptString;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using UnityEngine;


public interface IEncrypto {
      void Encrypt(string textToEncrypt, string tokenPassword, out string textEncrypted, out string textIV);
      void Decrypt(string textToDecrypt, string textIV, string tokenPassword, out string textDecypted);
}

public abstract class EncryptorMono :MonoBehaviour, IEncrypto
{

    public abstract void Decrypt(string textToDecrypt, string textIV, string tokenPassword, out string textDecypted);

  

    public abstract void Encrypt(string textToEncrypt, string tokenPassword, out string textEncrypted, out string textIV);
  
}

public class DraftEncryptorMono : MonoBehaviour
{
    public string txtPrivateKey;
    public string txtString;
    [Space]
    public string txtEncrypted;
    public string txtIV;
    [Space]
    public string txtDecrypted;

    public EncryptorMono m_encryption;

    [ContextMenu("Encrypt")]
    public void EncryptGiventData()
    {
        m_encryption.Encrypt(txtString, txtPrivateKey, out txtEncrypted, out txtIV);

    }


    [ContextMenu("Decrypt")]
    public void DecryptGiventData()
    {
        m_encryption.Decrypt(txtEncrypted, txtIV, txtPrivateKey, out txtDecrypted);

    }
   

  

   
}
