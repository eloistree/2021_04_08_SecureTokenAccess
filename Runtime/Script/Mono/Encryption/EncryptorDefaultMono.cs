using EncryptString;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using UnityEngine;

public class EncryptorDefaultMono : EncryptorMono
{

    public string m_macAddress;
    public EncryptionAlgorithm m_encryptAlgo;

    public  int m_sizeOfRijndael = 256/8;//128bits, 192 and 256
    public  int m_sizeOfTipleDES = 168/8; //168bits, 112 or 56
    public  int m_sizeOfDES = 64/8;//64bits
    public  int m_sizeOfRC2 = 64/8;//40bits to 124bits

    private void Awake()
    {
        m_macAddress = "";
    }
    public override void Encrypt(string textToEncrypt, string tokenPassword, out string textEncrypted, out string textIV)
    {
        string valideKey = GeneratePrivateKeyWithGoodSize(tokenPassword, m_encryptAlgo);
        EncryptionAlgorithm alg = m_encryptAlgo;
        Encryptor en = new Encryptor(alg, valideKey);
        byte[] iv;
        en.Encrypt(textToEncrypt, out textEncrypted, out iv);
        textIV = Convert.ToBase64String(iv);

    }

    public override void Decrypt(string textToDecrypt, string textIV, string tokenPassword, out string textDecypted )
    {
        EncryptionAlgorithm alg = m_encryptAlgo;

        string valideKey = GeneratePrivateKeyWithGoodSize(tokenPassword, m_encryptAlgo);
        byte[] IV = Convert.FromBase64String(textIV);
        Decryptor dec = new Decryptor(alg, IV);
        dec.Decrypt(textToDecrypt, valideKey, out textDecypted);
    }

  

    private string GeneratePrivateKeyWithGoodSize(string tokenPassword , EncryptionAlgorithm crypto)
    {
        int lenghtOfPassword = GetPasswordLenghtFor(crypto);
        string m_usePrivateKey = tokenPassword;
        if (m_usePrivateKey.Length > lenghtOfPassword)
            m_usePrivateKey = m_usePrivateKey.Substring(0, lenghtOfPassword);
        if (m_usePrivateKey.Length < lenghtOfPassword)
        {

            if (string.IsNullOrEmpty(m_macAddress))
                m_macAddress = FetchMacId();
            if (string.IsNullOrEmpty(m_macAddress))
                throw new NotImplementedException("Should be replace by something else that is stable on the PC");
                //PS: I AM USING THE MAC ADDRESSS OF THE USER TO BE SURE THAT THE HASH I PROPER TO EACH PC.
            if (m_macAddress.Length < 1)
                throw new Exception("Ok, apparently I don't have access to the mac address of this PC");

            int i = 0;
            while (m_usePrivateKey.Length< lenghtOfPassword)
            {
                m_usePrivateKey += m_macAddress[i%m_macAddress.Length];
                i++;
            }
        }
        if(m_usePrivateKey.Length!= lenghtOfPassword)
            throw new Exception("Private key must be "+ lenghtOfPassword);
        return m_usePrivateKey;
    }

    private int GetPasswordLenghtFor(EncryptionAlgorithm crypto)
    {
        switch (crypto)
        {
            case EncryptionAlgorithm.DES: return m_sizeOfDES;
            case EncryptionAlgorithm.Rc2: return m_sizeOfRC2;
            case EncryptionAlgorithm.Rijndael: return m_sizeOfRijndael;
            case EncryptionAlgorithm.TripleDes:return m_sizeOfTipleDES;
            default:return 8;
        }
    }

    public static string FetchMacId()
    {
        string macAddresses = "";
        foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
        {
            if (nic.OperationalStatus == OperationalStatus.Up)
            {
                macAddresses += nic.GetPhysicalAddress().ToString();
                break;
            }
        }
        return macAddresses;
    }
    public void SetEncryptionAlgorithm(int enumIndex)
    {
        try
        {
            EncryptionAlgorithm algo = (EncryptionAlgorithm)enumIndex;
        }
        catch (Exception) { };
    }
    public void SetEncryptionAlgorithm(EncryptionAlgorithm enumIndex)
    {
        m_encryptAlgo = enumIndex;
    }
    private void OnValidate()
    {
        m_sizeOfRijndael = 256 / 8;//128bits, 192 and 256
        m_sizeOfTipleDES = 168 / 8; //168bits, 112 or 56
        m_sizeOfDES = 64 / 8;//64bits
        m_sizeOfRC2 = 64 / 8;//40bits to 124bits
    }
}
