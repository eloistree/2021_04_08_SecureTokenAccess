using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class TokenStorageInFileDefaultMono : TokenStorageInStringableMono
{
    public EncryptorMono m_encryption;
    public EncryptionTokenStorage m_currentStorage;

    public override void GetStorageOf(string tokenId, string tokenPassword, out string storageTokenText)
    {
        storageTokenText = "";
        bool found;
        EncryptionTokenStorageValue value;
        m_currentStorage.GetStored(tokenId, out found, out value);
        if (found && value!=null) {

            try
            {
                m_encryption.Decrypt(value.m_encryptedValue, value.m_encryptedValueIV, tokenPassword, out storageTokenText);
            }
            catch (Exception e) {
                storageTokenText = "";
            }
        }
        else storageTokenText = "";
    }

  

    public override void SetStorage(string tokenId, string tokenPassword, string tokenText)
    {
        string txt, txtIV;
        m_encryption.Encrypt(tokenText, tokenPassword, out txt, out txtIV);
        m_currentStorage.SetOrAdd(tokenId, txt, txtIV);
    }

    public override string GetTextExport()
    {
        return JsonUtility.ToJson(m_currentStorage);
    }
    public override void SetTextWithExport(string text)
    {
        if (string.IsNullOrWhiteSpace(text))
            m_currentStorage = new EncryptionTokenStorage();
        try
        {
            m_currentStorage = JsonUtility.FromJson<EncryptionTokenStorage>(text);
        }
        catch (Exception) {
            m_currentStorage = new EncryptionTokenStorage();
        }
    }

    public override void Clear()
    {
        m_currentStorage.Clear();
    }

    public override List<string> GetListOfIdentifier()
    {
        return m_currentStorage.GetListOfIdentifier();
    }
}



