using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public abstract class StingableStorageMono : MonoBehaviour {


    public abstract string GetStoredText();

    public abstract void SaveToStoreText(string text);
}

public abstract class TokenStorageInStringableMono : TokenStorageMono
{

    public StingableStorageMono m_storageType;
    public abstract string GetTextExport();

    public abstract void SetTextWithExport(string text);

    private void Awake()
    {
        LoadOrCreateFile();

    }
    private void Start()
    {
        LoadOrCreateFile();

    }

    private void OnDestroy()
    {
        SaveOrOverrideFile();
    }


    public void SaveOrOverrideFile()
    {

        m_storageType.SaveToStoreText(GetTextExport());
    }
    public void LoadOrCreateFile()
    {

       SetTextWithExport( m_storageType.GetStoredText());
    }


}
