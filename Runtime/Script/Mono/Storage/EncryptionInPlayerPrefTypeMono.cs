using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EncryptionInPlayerPrefTypeMono : StingableStorageMono
{
    public string m_playerPrefIdStorage="";
    private void Reset()
    {
        m_playerPrefIdStorage = "TokenStorage" + UnityEngine.Random.value * 10000000000;
    }

    public override string GetStoredText()
    {
        return PlayerPrefs.GetString(m_playerPrefIdStorage);
    }

    public override void SaveToStoreText(string text)
    {
        PlayerPrefs.SetString(m_playerPrefIdStorage, text);
    }
}
