using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[System.Serializable]
public class EncryptionTokenStorage
{

    public List<EncryptionTokenStorageValue> m_encryptedStorage = new List<EncryptionTokenStorageValue>();

    public void SetOrAdd(string tokenId, string txt, string txtIV)
    {
        bool found;
        EncryptionTokenStorageValue value;
        GetStored(tokenId, out found, out value);

        if (found)
        {
            value.SetWith(tokenId, txt, txtIV);
        }
        else
        {
            AddWithoutChecking(tokenId, txt, txtIV);
        }
    }

    private void AddWithoutChecking(string tokenId, string txt, string txtIV)
    {
        m_encryptedStorage.Add(new EncryptionTokenStorageValue(tokenId, txt, txtIV));
    }

    public void GetStored(string tokenId, out bool found, out EncryptionTokenStorageValue value)
    {
        for (int i = 0; i < m_encryptedStorage.Count; i++)
        {
            if (m_encryptedStorage[i].m_tokenName.Length == tokenId.Length &&
                m_encryptedStorage[i].m_tokenName.IndexOf(tokenId) == 0)
            {
                found = true;
                value = m_encryptedStorage[i];
                return;
            }
        }

        found = false;
        value = null;
    }

    public void Clear()
    {
        m_encryptedStorage.Clear();
    }

    public List<string> GetListOfIdentifier()
    {
       return  m_encryptedStorage.Select(k => k.m_tokenName).ToList();
    }
}