
[System.Serializable]
public class EncryptionTokenStorageValue
{
    public string m_tokenName;
    public string m_encryptedValue;
    public string m_encryptedValueIV;

    public EncryptionTokenStorageValue(string tokenId, string txt, string txtIV)
    {
        SetWith(tokenId, txt, txtIV);
    }
    public void SetWith(string tokenId, string txt, string txtIV)
    {
        m_tokenName = tokenId;
        m_encryptedValue = txt;
        m_encryptedValueIV = txtIV;
    }
}

