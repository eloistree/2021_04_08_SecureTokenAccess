using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class EncryptionInOneFileTypeMono : StingableStorageMono
{
    public StingableStorageMono m_whereToStore;
    public string m_absolutePathFileDefault;
    public string m_absolutePathFileCustom;

    public string m_playerPrefIdStorage = "TokenStorageID";

    public override string GetStoredText()
    {
        LoadStorageTarget();
        string path = GetStorageFileTarget();
        if (File.Exists(path))
            return File.ReadAllText(path);
        else return "";
    }

    public override void SaveToStoreText(string text)
    {
        File.WriteAllText(GetStorageFileTarget(), text);
    }


    private void LoadStorageTarget()
    {
        m_absolutePathFileDefault = Application.dataPath + "/default.tokenstorage";
        m_absolutePathFileCustom = (PlayerPrefs.GetString(m_playerPrefIdStorage));
    }
    private void Reset()
    {
        m_playerPrefIdStorage = "TokenStorageID" + UnityEngine.Random.value * 10000000000;
    }

    private void Awake()
    {
        LoadStorageTarget();

    }

    private void OnDestroy()
    {
        SaveStorageTarget();
    }

   

    public string GetStorageFileTarget()
    {

        if (m_absolutePathFileCustom != null && m_absolutePathFileCustom.Length > 0)
            return m_absolutePathFileCustom;
        return m_absolutePathFileDefault;
    }
    private void SaveStorageTarget()
    {
        PlayerPrefs.SetString(m_playerPrefIdStorage, m_absolutePathFileCustom);
    }


    public void OpenFileWithUnity()
    {
        Application.OpenURL(GetStorageFileTarget());
    }
    public void SetCustomPath(string absolutePath)
    {
        m_absolutePathFileCustom = absolutePath;
        SaveStorageTarget();
    }
    public string GetCustomPath()
    {
        return m_absolutePathFileCustom;
    }
}
