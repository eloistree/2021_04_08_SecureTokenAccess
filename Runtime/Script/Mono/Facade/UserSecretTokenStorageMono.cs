using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;



public class UserSecretTokenStorageMono : MonoBehaviour, IUserSecretTokenStorage
{
    public TokenStorageInStringableMono m_storage;
    public DefaultRandomPasswordGenerator m_defaultPasswordGenerator;
    public RandomPasswordGenerator m_customGenerator;
    public void SetCustomPasswordGenerator(RandomPasswordGenerator generator) {
        m_customGenerator = generator;
    }
    public RandomPasswordGenerator GetPasswordGenerator()
    {
        if (m_customGenerator != null)
            return m_customGenerator;
        return m_defaultPasswordGenerator;

    }


    public void GenerateRandom(string tokenId, string tokenPassword)
    {
        string pw = GetPasswordGenerator().GetRandomPassword();
        SetStorage(tokenId, tokenPassword, pw);
    }

    public void GenerateRandom(string tokenId, string tokenPassword, string allowChars)
    {
        string pw = m_defaultPasswordGenerator.GetRandomPasswordFrom(allowChars);
        SetStorage(tokenId, tokenPassword, pw);
    }

    public void GetStorageOf(string tokenId, string tokenPassword, out string storageTokenText)
    {
        m_storage.GetStorageOf(tokenId, tokenPassword, out storageTokenText);
    }

    public void SetStorage(string tokenId, string tokenPassword, string tokenText)
    {
        m_storage.SetStorage(tokenId, tokenPassword, tokenText);
    }
}

