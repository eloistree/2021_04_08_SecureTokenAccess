using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_FileStorageEncryptionToken : MonoBehaviour
{

    public EncryptionInOneFileTypeMono m_fileTokenStorage;
    public InputField m_path;
    
    void Start()
    {
        m_path.text = m_fileTokenStorage.GetCustomPath();


    }


    public void SetCustomPath(string absolutePath) {
        m_fileTokenStorage.SetCustomPath(absolutePath);
    }

    public void OpenCurrentUseFile() {
        m_fileTokenStorage.OpenFileWithUnity();
    }
}
