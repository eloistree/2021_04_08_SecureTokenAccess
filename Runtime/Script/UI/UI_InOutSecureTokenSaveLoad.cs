using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_InOutSecureTokenSaveLoad : MonoBehaviour
{
    public TokenStorageMono m_mono;
    public enum TypeInOutToken { Save,Load}
    public TypeInOutToken m_type;
    public InputField m_targetId;
    public InputField m_targetKey;
    public InputField m_text;



    public void DoTheThing()
    {
        if (m_type == TypeInOutToken.Save)
        {
            PushIn();
        }
        if (m_type == TypeInOutToken.Load)
        {
            LoadFrom();
        }
    }

    public void LoadFrom()
    {
        string s;
        m_mono.GetStorageOf(m_targetId.text, m_targetKey.text, out  s);
        m_text.text = s;
    }

    public void PushIn()
    {
        m_mono.SetStorage(m_targetId.text, m_targetKey.text, m_text.text);
    }

}
