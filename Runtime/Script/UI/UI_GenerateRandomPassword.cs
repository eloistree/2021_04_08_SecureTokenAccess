using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_GenerateRandomPassword : MonoBehaviour
{

    public void GeneratePassword()
    {
        m_mono.GenerateRandom(m_targetId.text, m_targetKey.text);
        string s;
        m_mono.GetStorageOf(m_targetId.text, m_targetKey.text, out s);
        m_textStoredByPassword.text = s;
    }

 
    public UserSecretTokenStorageMono m_mono;
    public InputField m_targetId;
    public InputField m_targetKey;
    public InputField m_textStoredByPassword;




   

}
