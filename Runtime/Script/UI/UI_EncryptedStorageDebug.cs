using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_EncryptedStorageDebug : MonoBehaviour
{
    public TokenStorageMono m_target;
    public InputField m_listStorageIds;

    public void Awake()
    {
        InvokeRepeating("Refresh", 0, 1);
    }

    public void Refresh() {
        List<string> ids = m_target.GetListOfIdentifier();
        m_listStorageIds.text = string.Join(" ", ids);
    }
    public void Clear() {
        m_target.Clear();
    }
}
