using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;
//SOURCE: https://www.codeproject.com/Articles/16920/Encrypt-and-Decrypt-a-String
//Amir Mehrabi-Jorshari under The Code Project Open License (CPOL)
namespace EncryptString
{
    public enum EncryptionAlgorithm
    {
         DES = 0, Rc2, Rijndael, TripleDes 
    }
    public class EncryptEngine
    {
        bool m_hasReceiveKey = false;
        EncryptionAlgorithm m_algoritmChoosed;
        string m_keyword = "";
        public byte[] m_vector;
        public EncryptEngine(EncryptionAlgorithm algorithmChoosed, string key)
        {
            if (key.Length == 0)
                m_hasReceiveKey = false;
            else
                m_hasReceiveKey = true;

            m_keyword = key;
            m_algoritmChoosed = algorithmChoosed;
        }

        public EncryptionAlgorithm EncryptionAlgorithm
        {
            get
            {
                return m_algoritmChoosed;
            }
            set
            {
                m_algoritmChoosed = value;
            }
        }
    
        public ICryptoTransform GetCryptTransform()
        {
            byte[] key = Encoding.ASCII.GetBytes(m_keyword);

            switch (m_algoritmChoosed)
            {
                case EncryptionAlgorithm.DES:
                    DES des = new DESCryptoServiceProvider();
                    des.Mode = CipherMode.CBC;
                    if (m_hasReceiveKey) des.Key = key;
                    m_vector = des.IV;
                    return des.CreateEncryptor();
                case EncryptionAlgorithm.Rc2:
                    RC2 rc =new RC2CryptoServiceProvider();
                    rc.Mode = CipherMode.CBC;
                    if (m_hasReceiveKey) rc.Key = key;
                    m_vector = rc.IV;
                    return rc.CreateEncryptor();
                case EncryptionAlgorithm.Rijndael:
                    Rijndael rj = new RijndaelManaged();
                    rj.Mode = CipherMode.CBC;
                    if (m_hasReceiveKey) rj.Key = key;
                    m_vector = rj.IV;
                    return rj.CreateEncryptor();
                case EncryptionAlgorithm.TripleDes:
                    TripleDES tDes = new TripleDESCryptoServiceProvider();
                    tDes.Mode = CipherMode.CBC;
                    if (m_hasReceiveKey) tDes.Key = key;
                    m_vector = tDes.IV;
                    return tDes.CreateEncryptor();
                default:
                    throw new CryptographicException("Algorithm " + m_algoritmChoosed + " Not Supported!");
            }
        }
        public static bool ValidateKeySize(EncryptionAlgorithm algID,int lenght)
        {
            switch (algID)
            {
                case EncryptionAlgorithm.DES:
                    DES des = new DESCryptoServiceProvider();
                    return des.ValidKeySize(lenght);
                case EncryptionAlgorithm.Rc2:
                    RC2 rc = new RC2CryptoServiceProvider();
                    return rc.ValidKeySize(lenght);
                case EncryptionAlgorithm.Rijndael:
                    Rijndael rj = new RijndaelManaged();
                    return rj.ValidKeySize(lenght);
                case EncryptionAlgorithm.TripleDes:
                    TripleDES tDes = new TripleDESCryptoServiceProvider();
                    return tDes.ValidKeySize(lenght);
                default:
                    throw new CryptographicException("Algorithm " + algID + " Not Supported!");
            }
        }
    }

   
}
