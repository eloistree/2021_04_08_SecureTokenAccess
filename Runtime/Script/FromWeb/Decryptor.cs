using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using UnityEngine;

//SOURCE: https://www.codeproject.com/Articles/16920/Encrypt-and-Decrypt-a-String
//Amir Mehrabi-Jorshari under The Code Project Open License (CPOL)
namespace EncryptString
{
    public class Decryptor
    {
        EncryptionAlgorithm m_algoritmChoosed;
        byte[] m_IV;
        public Decryptor(EncryptionAlgorithm algID)
        {
            m_algoritmChoosed = algID;
        }
        public Decryptor(EncryptionAlgorithm algID, byte[] iv)
        {
            m_algoritmChoosed = algID;
            m_IV = iv;
        }

   

        public void Decrypt(string textToDecrypt, string key, out string decrypted)
        {
            DecryptTransformer dt = new DecryptTransformer(m_algoritmChoosed, m_IV);
            dt.SetSecurityKey(key);

            byte[] buffer = Convert.FromBase64String(textToDecrypt.Trim());
            MemoryStream ms = new MemoryStream(buffer);

            // Create a CryptoStream using the memory stream and the 
            // CSP DES key. 
            CryptoStream encStream = new CryptoStream(ms, dt.GetCryptoTransform(), CryptoStreamMode.Read);

            // Create a StreamReader for reading the stream.
            StreamReader sr = new StreamReader(encStream);

            // Read the stream as a string.
            string val = sr.ReadLine();

            // Close the streams.
            sr.Close();
            encStream.Close();
            ms.Close();

            decrypted= val;


        }
    }
}