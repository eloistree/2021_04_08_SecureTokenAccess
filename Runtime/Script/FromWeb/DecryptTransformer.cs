using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;

//SOURCE: https://www.codeproject.com/Articles/16920/Encrypt-and-Decrypt-a-String
//Amir Mehrabi-Jorshari under The Code Project Open License (CPOL)
namespace EncryptString
{
    public class DecryptTransformer
    {
        EncryptionAlgorithm m_algorithmChoosed;
        string m_securityKey = "";
        Byte[] m_IV;
        bool m_hasReceivedIV = false;
        public DecryptTransformer(EncryptionAlgorithm algID)
        {
            m_algorithmChoosed = algID;
        }
        public DecryptTransformer(EncryptionAlgorithm algID, byte[] iv)
        {
            m_algorithmChoosed = algID;
            m_IV = iv;
            m_hasReceivedIV = true;
        }

        public EncryptionAlgorithm EncryptionAlgorithm
        {
            get
            {
                return m_algorithmChoosed;
            }
            set
            {
                m_algorithmChoosed = value;
            }
        }

        public void SetSecurityKey(string Key)
        {
            m_securityKey = Key;
        }
        public ICryptoTransform GetCryptoTransform()
        {
            bool bHasSecuityKey = false;
            if (m_securityKey.Length != 0)
                bHasSecuityKey = true;

            byte[] key = Encoding.ASCII.GetBytes(m_securityKey);
            switch (m_algorithmChoosed)
            {
                case EncryptionAlgorithm.DES:
                    DES des = new DESCryptoServiceProvider();
                    if (bHasSecuityKey) des.Key = key;
                    if (m_hasReceivedIV) des.IV = m_IV;
                    return des.CreateDecryptor();

                case EncryptionAlgorithm.Rc2:
                    RC2 rc = new RC2CryptoServiceProvider();
                    if (bHasSecuityKey) rc.Key = key;
                    if (m_hasReceivedIV) rc.IV = m_IV;
                    return rc.CreateDecryptor();
                case EncryptionAlgorithm.Rijndael:
                    Rijndael rj = new RijndaelManaged();
                    if (bHasSecuityKey) rj.Key = key;
                    if (m_hasReceivedIV) rj.IV = m_IV; ;
                    return rj.CreateDecryptor();
                case EncryptionAlgorithm.TripleDes:
                    TripleDES tDes = new TripleDESCryptoServiceProvider();
                    if (bHasSecuityKey) tDes.Key = key;
                    if (m_hasReceivedIV) tDes.IV = m_IV;
                    return tDes.CreateDecryptor();
                default:
                    throw new CryptographicException("Algorithm ID '" + m_algorithmChoosed + "' not supported.");
            }
        }

    }
}
