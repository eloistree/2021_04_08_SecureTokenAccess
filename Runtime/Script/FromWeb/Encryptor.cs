using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;

//SOURCE: https://www.codeproject.com/Articles/16920/Encrypt-and-Decrypt-a-String
//Amir Mehrabi-Jorshari under The Code Project Open License (CPOL)
namespace EncryptString
{
    public class Encryptor
    {
        EncryptEngine m_engin;
        public Encryptor(EncryptionAlgorithm alogrithmChoosed, string key)
        {
            m_engin = new EncryptEngine(alogrithmChoosed, key);
        }

        public EncryptEngine EncryptEngine
        {
            get
            {
                return m_engin;
            }
            set
            {
                m_engin = value;
            }
        }
    
        public void Encrypt(string textToEncrypt, out string text, out byte [] IV)
        {
            MemoryStream memory = new MemoryStream();
            CryptoStream stream = new CryptoStream(memory, m_engin.GetCryptTransform(), CryptoStreamMode.Write);
            StreamWriter streamwriter = new StreamWriter(stream);
            streamwriter.WriteLine(textToEncrypt);
            streamwriter.Close();
            stream.Close();
            IV = m_engin.m_vector;
            byte[] buffer = memory.ToArray();
            memory.Close();
            text = Convert.ToBase64String(buffer);

        }
    }
}
