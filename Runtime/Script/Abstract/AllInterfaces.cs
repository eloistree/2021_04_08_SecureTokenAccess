using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUserSecretTokenStorage : ITokenStorage
{

     void GenerateRandom(string tokenId, string tokenPassword);
     void GenerateRandom(string tokenId, string tokenPassword, string allowChars);


}
public interface ITokenStorage
{

     void GetStorageOf(string tokenId, string tokenPassword, out string storageTokenText);
     void SetStorage(string tokenId, string tokenPassword, string tokenText);


}
public interface RandomPasswordGenerator
{

     string GetRandomPassword();
}


public abstract class TokenStorageMono : MonoBehaviour, ITokenStorage
{
    public abstract void GetStorageOf(string tokenId, string tokenPassword, out string storageTokenText);
    public abstract void SetStorage(string tokenId, string tokenPassword, string tokenText);

    public abstract void Clear();
    public abstract List<string> GetListOfIdentifier();
}